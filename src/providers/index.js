import axios from 'axios'

const http = axios.create({
    baseURL: 'https://content.guardianapis.com/',
    timeout: 1000,
});


export default http;
