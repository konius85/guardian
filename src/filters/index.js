import pathNameSlice from "@/filters/pathNameSlice";
import dateFormat from "@/filters/dateFormat";

export default {
    install(Vue) {
        Vue.filter('pathNameSlice', pathNameSlice);
        Vue.filter('dateFormat', dateFormat);
    }
}
