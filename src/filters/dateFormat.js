export default function (value) {
    if (!value) return ''
    return new Date(value).toLocaleString()
}
