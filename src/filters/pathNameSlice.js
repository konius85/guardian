export default function (value) {
    if (!value) return ''
    const path = value.split('/')
    return path.length > 1 ? path[1] : ''
}
