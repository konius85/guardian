import http from '../../providers'
import utils from "@/utils/utils";

const apiKey = '438dac33-b38f-4f10-99fd-975fac25f42e';
const favoriteLocalStorageKey = 'favorites';
const state = {
    articles: [],
    favorites: [],
    totalArticles: 0,
}

const actions = {
    async fetchArticles({commit}, payload) {
        const response = await http.get(`/${payload.route}?page=${payload.page}&page-size=10&api-key=${apiKey}`)
        const { pages, results} = response.data.response;
        commit('setArticles', results)
        commit('setTotalArticles', pages)
    },
    fetchFavorites({commit}) {
        const favorites = utils.getFromLocalStorage(favoriteLocalStorageKey);
        commit('setFavorites', favorites)
    },
    addToFavorite({commit}, payload) {
        const localData = utils.getFromLocalStorage(favoriteLocalStorageKey);
        localData.push(payload);
        utils.setLocalStorage(favoriteLocalStorageKey, localData);
        commit('setFavorites', localData)
    },
    removeFromFavorite({commit}, payload) {
        const favorites = utils.getFromLocalStorage(favoriteLocalStorageKey)
        const itemsToSave = favorites.filter(f => f.id !== payload.id)
        utils.setLocalStorage(favoriteLocalStorageKey, itemsToSave)
        commit('setFavorites', itemsToSave)
    }
}

const getters = {
    articles: (state) => state.articles,
    totalArticles: (state) => state.totalArticles,
    favorites: (state) => state.favorites,
}

const mutations = {
    setArticles: (state, articles) => state.articles = articles,
    setFavorites: (state, favorites) => state.favorites = favorites,
    setTotalArticles: (state, totalArticles) => state.totalArticles = totalArticles
}

export default {
    state,
    actions,
    getters,
    mutations,
}
