const getFromLocalStorage = (key) => {
    return JSON.parse(localStorage.getItem(key) ?? '[]')
}

const setLocalStorage = (key, data) => {
    localStorage.setItem(key, JSON.stringify(data));
}


export default {
    getFromLocalStorage,
    setLocalStorage,
}
