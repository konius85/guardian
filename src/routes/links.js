export default {
    world: '/world',
    sport: '/sport',
    politics: '/politics',
    business: '/business',
    favorites: '/favorites',
}
