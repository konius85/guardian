import Vue from 'vue'
import VueRouter from 'vue-router'
import links from "@/routes/links";

Vue.use(VueRouter)

const alias = Object.values(links)

export default new VueRouter({
    mode: 'history',
    routes: [
        {
            path: `${links.world}/:page?`,
            component: () => import(/* webpackChunkName: "articles" */ '@/components/Main'),
            alias
        },
        { path: '*', redirect: links.world}
    ]
})
