import Vue from 'vue'
import App from './App.vue'
import store from './store'
import router from './routes'
import filters from "./filters"

Vue.config.productionTip = false
Vue.use(filters);

new Vue({
  store,
  router,
  render: h => h(App),
}).$mount('#app')
